﻿using System.Collections.Generic;
using System.Linq;

namespace BIT_AES_Core
{
    /// <summary>
    /// Usefull language extensions.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Splits an array into several smaller arrays. Source code copied from <see cref="https://stackoverflow.com/questions/18986129/c-splitting-an-array-into-n-parts"/>
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to split.</param>
        /// <param name="size">The size of the smaller arrays.</param>
        /// <returns>An array containing smaller arrays.</returns>
        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++) yield return array.Skip(i * size).Take(size);
        }
    }
}
