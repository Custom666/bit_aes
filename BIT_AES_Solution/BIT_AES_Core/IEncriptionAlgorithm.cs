﻿namespace BIT_AES_Core
{
    /// <summary>
    /// Encription algorithm interface designed to encrypt array of bytes with given key.
    /// </summary>
    public interface IEncriptionAlgorithm
    {
        /// <summary>
        /// Process specific algorithm based on byte input.
        /// </summary>
        /// <param name="message">Input message</param>
        /// <returns>Specific output to specific implemented algorithm as byte array.</returns>
        byte[] Proceed(byte[] message);

        /// <summary>
        /// Key is necessery for encription algorithm.
        /// </summary>
        byte[] Key { get; set; }
    }
}
