﻿using BIT_AES_Core;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace BIT_AES_ConsoleApp
{
    /// <summary>
    /// Console application for demonstrating AES encription and decription algorithm with 128 bits key. 
    /// </summary>
    internal class Program
    {
        private static readonly string OutputFilename = "output.txt";

        private static readonly string Help = "Program must be run with: [argument1 <argument2>] where\n\t" +
            "[argument1] is required argument:\n\t\t" +
            "-e use encryption algorithm\n\t\t" +
            "-d use decryption algorithm\n\t" +
            "<argument2> is optional argument:\n\t\t" +
            "text input wrapped in \"\" (in case of encryption, text should be any format. In case of decryption, text should be formated hexadecimal with spaces between characters.\n\t\t" +
            "For example \"A9 39 9A 30 5C 08 B2 EF DB A6 72 92 9A CF 17 88\")\n" +
            "If <argument2> is not specified then program will proccess input from file specified in appsettings.json. Otherwise program will run choosen algorithm from [argument1] with input text from <argument2>.";

        private static readonly string Done = "Done.";
        private static readonly string InvalidKey = $"Given key must be length of { AESHelper.StateLength } Bytes";

        private static readonly string KeyFilePathNotExist = "File with key did not found! Please change key_file_path in appsettings.json to correct file with key or add new file to current project directory. File with key must have .key extension.";
        private static readonly string PlainTextFilePathNotExist = "File with plain text did not found! Please change plain_text_file_path in appsettings.json to file that you want to encrypt or start this application with addition [-c] argument followed by string wrapped in \"\".\nFor example [-e -c \"Hellow World\"]";
        private static readonly string CipherTextFilePathNotExist = "File with cipher text did not found! Please change cipher_text_file_path in appsettings.json to file that you want to decrypt or start this application with addition [-c] argument followed by string wrapped in \"\"\nFor example [-d -c \"1d 3e 20 b3 99 52 6f b5 e8 53 e4 8d 32 5f 6b 41\"]";
        private static readonly string OuputFilePathNotExist = $"Output file path did not found! Please change output_file_path in appsettings.json to file that will containt algorithm results. Process will continue with default filename { OutputFilename } in current directory.";

        private static readonly string CiphetTextFileWrongFormat = $"File with cipher text is in wrong format! Please make sure that content is in hexadecimal string with spaces between each values.\nFor example [1d 3e 20 b3 99 52 6f b5 e8 53 e4 8d 32 5f 6b 41].";

        /// <summary>
        /// Entry point. 
        /// </summary>
        /// <param name="args">Application arguments.</param>
        public static void Main(string[] args)
        {
            IEncriptionAlgorithm aes = null;

            // read configuration from appsettings.json file
            var configuration = new ConfigurationBuilder()
                          .SetBasePath(Directory.GetCurrentDirectory())
                          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                          .Build();

            // recognize input 
            var inputResult = proceedInput(args);

            // invalid input
            if (!inputResult.HasValue)
            {
                Console.WriteLine(Help);
                return;
            }

            // load key from file spicified in configuration file
            var key = loadKey(configuration);

            if (key == null) return;

            // initialize type of process from input 
            switch (inputResult.Value.Item1)
            {
                // initialize aes encryptor
                case InputProcessType.Encrypt: aes = new AESEncryptor(key); break;

                // initialize aes decryptor
                case InputProcessType.Decrypt: aes = new AESDecryptor(key); break;
            }

            byte[] cipher = null;

            try
            {
                // prepare text for processing
                var message = loadMessage(configuration, inputResult.Value, inputResult.Value.Item2 == InputTextFrom.Console ? args[1] : string.Empty);
                
                // proceed aes algorithm with prepared message
                cipher = aes.Proceed(message);
            }
            catch (FormatException)
            {
                Console.WriteLine(CiphetTextFileWrongFormat);

                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return;
            }

            string result = null;

            // prepare encription/decription result into output string
            if (inputResult.Value.Item1 == InputProcessType.Encrypt)
            {
                var builder = new StringBuilder();

                foreach (var b in cipher) builder.Append(b.ToString("X2") + " ");

                builder.Remove(builder.Length - 1, 1);

                result = builder.ToString();

            }
            else if (inputResult.Value.Item1 == InputProcessType.Decrypt)
            {
                result = Encoding.ASCII.GetString(cipher);
            }

            // print output string into file/console based on intial input
            if (inputResult.Value.Item2 == InputTextFrom.Console)
            {
                Console.WriteLine(result);
            }
            else
            {
                var outputFilename = configuration.GetSection("output_file_path").Value;

                if (outputFilename == null || string.IsNullOrEmpty(outputFilename))
                {
                    Console.WriteLine(OuputFilePathNotExist);

                    outputFilename = OutputFilename;
                }

                File.WriteAllText(outputFilename, result);
            }

            Console.WriteLine(Done);
        }

        private static (InputProcessType, InputTextFrom)? proceedInput(string[] args)
        {
            if (args == null || args.Length <= 0 || string.IsNullOrEmpty(args[0])) return null;

            switch (args[0])
            {
                case "-e": return args.Length >= 2 && !string.IsNullOrEmpty(args[1]) ? (InputProcessType.Encrypt, InputTextFrom.Console) : (InputProcessType.Encrypt, InputTextFrom.File);
                case "-d": return args.Length >= 2 && !string.IsNullOrEmpty(args[1]) ? (InputProcessType.Decrypt, InputTextFrom.Console) : (InputProcessType.Decrypt, InputTextFrom.File);
                default: return null;
            }
        }

        private static byte[] loadMessage(IConfigurationRoot configuration, (InputProcessType, InputTextFrom) inputResult, string text)
        {
            var isEncriptionProcess = inputResult.Item1 == InputProcessType.Encrypt;
            var isTextFromConsole = inputResult.Item2 == InputTextFrom.Console;

            // prepare text from console for encription process
            if (isEncriptionProcess && isTextFromConsole)
            {
                return Encoding.ASCII.GetBytes(text);
            }
            // prepare text from console for decription process
            else if (!isEncriptionProcess && isTextFromConsole)
            {
                return text.Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
            }
            // prepare text from file for encription process
            else if (isEncriptionProcess && !isTextFromConsole)
            {
                var messageFilePath = configuration.GetSection("plain_text_file_path").Value;

                if (messageFilePath == null || string.IsNullOrEmpty(messageFilePath) || !File.Exists(messageFilePath))
                {
                    Console.WriteLine(PlainTextFilePathNotExist);

                    return null;
                }

                return File.ReadAllBytes(messageFilePath);
            }
            // prepare text from file for decription process
            else if (!isEncriptionProcess && !isTextFromConsole)
            {
                var messageFilePath = configuration.GetSection("cipher_text_file_path").Value;

                if (messageFilePath == null || string.IsNullOrEmpty(messageFilePath) || !File.Exists(messageFilePath))
                {
                    Console.WriteLine(CipherTextFilePathNotExist);

                    return null;
                }

                return File.ReadAllText(messageFilePath).Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
            }

            return null;
        }

        private static byte[] loadKey(IConfigurationRoot configuration)
        {
            var keyFilePath = configuration.GetSection("key_file_path").Value;

            if (keyFilePath == null || string.IsNullOrEmpty(keyFilePath)) keyFilePath = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.key", SearchOption.TopDirectoryOnly).FirstOrDefault();

            if (keyFilePath == null || !File.Exists(keyFilePath))
            {
                Console.WriteLine(KeyFilePathNotExist);
                return null;
            }

            var key = File.ReadAllText(keyFilePath);

            if (string.IsNullOrEmpty(key) || key.Length < AESHelper.StateLength)
            {
                Console.WriteLine(InvalidKey);
                return null;
            }

            return key.Split(' ').Select(item => Convert.ToByte(item, 16)).ToArray();
        }
    }

    internal enum InputProcessType
    {
        Encrypt, Decrypt
    }

    internal enum InputTextFrom
    {
        Console, File
    }
}
